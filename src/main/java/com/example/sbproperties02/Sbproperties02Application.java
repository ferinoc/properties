package com.example.sbproperties02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sbproperties02Application {

    public static void main(String[] args) {
        SpringApplication.run(Sbproperties02Application.class, args);
    }
}
