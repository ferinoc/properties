package com.example.sbproperties02;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.example.sbproperties02.PropertiesUtil.getBooleanMarket;
import static com.example.sbproperties02.PropertiesUtil.getByMarket;

@Configuration
@Component
@ConfigurationProperties(prefix = "license-plate")
public class LicensePlateProperties {

    private Map<String, Boolean> enabledFlags;
    private Map<String, String> urls;
    private Map<String, String> tokens;
    private String mockedVin;
    private Map<String, Integer> timeouts;
    private Map<String, Boolean> xmlResponses;
    private Map<String, String> proxyUrls;

    public Boolean getEnabled(String marketCode) {
        return getBooleanMarket(enabledFlags, marketCode);
    }

    public String getUrl(String marketCode) {
        return getByMarket(urls, marketCode);
    }

    public String getToken(String marketCode) {
        return getByMarket(tokens, marketCode);
    }

    public String getMockedVin() {
        return mockedVin;
    }

    public Integer getTimeout(String marketCode) {
        return getByMarket(timeouts, marketCode);
    }

    public boolean getXmlResponse(String marketCode) {
        return getBooleanMarket(xmlResponses, marketCode);
    }

    public String getProxyUrl(String marketCode) {
        return getByMarket(proxyUrls, marketCode);
    }

    public Map<String, Boolean> getEnabledFlags() {
        return enabledFlags;
    }

    public void setEnabledFlags(Map<String, Boolean> enabledFlags) {
        this.enabledFlags = enabledFlags;
    }

    public Map<String, String> getUrls() {
        return urls;
    }

    public void setUrls(Map<String, String> urls) {
        this.urls = urls;
    }

    public Map<String, String> getTokens() {
        return tokens;
    }

    public void setTokens(Map<String, String> tokens) {
        this.tokens = tokens;
    }

    public void setMockedVin(String mockedVin) {
        this.mockedVin = mockedVin;
    }

    public Map<String, Integer> getTimeouts() {
        return timeouts;
    }

    public void setTimeouts(Map<String, Integer> timeouts) {
        this.timeouts = timeouts;
    }

    public Map<String, Boolean> getXmlResponses() {
        return xmlResponses;
    }

    public void setXmlResponses(Map<String, Boolean> xmlResponses) {
        this.xmlResponses = xmlResponses;
    }

    public Map<String, String> getProxyUrls() {
        return proxyUrls;
    }

    public void setProxyUrls(Map<String, String> proxyUrls) {
        this.proxyUrls = proxyUrls;
    }
}
