package com.example.sbproperties02.controller;

import com.example.sbproperties02.ConfigProperties;
import com.example.sbproperties02.LicensePlateProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/fce")
public class HelloController {

    private final ConfigProperties configProperties;

    private final LicensePlateProperties licensePlateProperties;

    @Autowired
    public HelloController(ConfigProperties configProperties, LicensePlateProperties licensePlateProperties) {
        this.configProperties = configProperties;
        this.licensePlateProperties = licensePlateProperties;
    }

    @GetMapping("/")
    public ResponseEntity<String> index() {

        return ResponseEntity.ok("hello ");
    }

    @GetMapping("/market")
    public ResponseEntity<String> sayHello(@RequestParam(value = "marketCode", required = false) final String marketCode) {
        String header = configProperties.getHeaderByMarket(marketCode);

        Boolean enabled = licensePlateProperties.getEnabled(marketCode);

        return ResponseEntity.ok("header: " + header + " License plate enabled: " + enabled);
    }

}
