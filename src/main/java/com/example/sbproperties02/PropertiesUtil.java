package com.example.sbproperties02;

import java.util.Map;

public final class PropertiesUtil {

    private static final String DEFAULT_KEY = "default";

    private PropertiesUtil() {
    }

    public static boolean getBooleanMarket(Map<String, Boolean> list, String market) {
        return Boolean.TRUE.equals(getByMarket(list, market));
    }

    public static <T> T getByMarket(Map<String, T> map, String market) {
        if (map == null) {
            return null;
        }

        if (market != null) {
            T byMarket = map.get(market.toUpperCase());
            if (byMarket != null) {
                return byMarket;
            }

            byMarket = map.get(market.toLowerCase());
            if (byMarket != null) {
                return byMarket;
            }
        }

        return map.get(DEFAULT_KEY);
    }

}
